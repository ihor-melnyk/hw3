const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
    const schema = Joi.object({
        password: Joi.string()
            .min(6)
            .max(20)
            .required(),
        email: Joi.string()
            .email()
            .required(),
        role: Joi.string()
            .equal('DRIVER','SHIPPER')
            .required(),
    });

    try {
        await schema.validateAsync(req.body);
        next();
    }
    catch (err) {
        next(err);
    }
};

const truckValidator = async (req, res, next) => {
    const schema = Joi.object({
        type: Joi.string()
            .equal('SPRINTER','SMALL STRAIGHT','LARGE STRAIGHT')
            .required(),
    });

    try {
        await schema.validateAsync(req.body);
        next();
    }
    catch (err) {
        next(err);
    }
};

module.exports = {
    registrationValidator,
    truckValidator
}