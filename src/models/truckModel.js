const { string } = require('joi');
const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    assignedTo: {
        type: mongoose.Schema.Types.ObjectId,
        required: false
    },
    type: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
        default: 'IS',
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { Truck };
