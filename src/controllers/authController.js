const express = require('express'); 
const router = express.Router();

const {
    registration,
    signIn,
    sendPasswordEmail
} = require('../services/authService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    registrationValidator
} = require('../middlewares/validationMidlleware');

router.post('/register',registrationValidator, asyncWrapper(async (req, res) => {
    const {
        email,
        password,
        role
    } = req.body;

    await registration({email, password, role});

    res.json({message: 'Profile created successfully!'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const {
        email,
        password,
    } = req.body;

    const token = await signIn({email, password});
    res.json({jwt_token:token, message: 'Logged in successfully!'})
}));

router.post('/forgot_password', asyncWrapper(async (req, res) => {
    const {
        email,
    } = req.body;

    await sendPasswordEmail(email).catch((err)=>{console.log(err)});

    res.json({message: 'New password sent to your email address'})
}));

module.exports = {
    authRouter: router
}