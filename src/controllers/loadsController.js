const express = require('express');
const { shipperMiddleware, driverMiddleware } = require('../middlewares/authMiddleware');
const router = express.Router();

const {
    addLoadToUser,
    getLoadsByUserId,
    postLoadByIdAndSearchDriver,
    getActiveLoadForUser,
    iterateLoadState,
    getLoadByIdForUser,
    updateLoadByIdForUser,
    deleteLoadByIdForUser,
    getShippingInfoByIdForUser
} = require('../services/loadsService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const loads = await getLoadsByUserId(userId,req.query.offset,req.query.limit,req.query.status);
    res.json({loads});
}));


router.get('/active', driverMiddleware, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const load = await getActiveLoadForUser(userId);
    res.json({load});
}));

router.patch('/active/state', driverMiddleware,asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const newState = await iterateLoadState(userId);
    res.json({message: `Load state changed to ${newState}`});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const load = await getLoadByIdForUser(id);
    res.json({load});
}));

router.get('/:id/shipping_info', shipperMiddleware,asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const info = await getShippingInfoByIdForUser(id);
    res.json(info);
}));

router.post('/', shipperMiddleware, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    await addLoadToUser(userId, req.body);
    res.json({message: "Load created successfully"});
}));

router.put('/:id', shipperMiddleware,asyncWrapper(async (req, res) => { 
    const { id } = req.params;
    const data = req.body;
    await updateLoadByIdForUser(id, data);
    res.json({message: "Load updated successfully"});
}));

router.post('/:id/post', shipperMiddleware,asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    let status = await postLoadByIdAndSearchDriver(id, userId);
    res.json({message: "Load posted successfully",driver_found:status});
}));

router.delete('/:id', shipperMiddleware,asyncWrapper(async (req, res) => {
    const { id } = req.params;
    await deleteLoadByIdForUser(id);
    res.json({message: "Load deleted successfully"});
}));

module.exports = {
    loadsRouter: router
}