const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const { getTruckByIdForUser } = require('./trucksService');
const { getUserById } = require('./usersService');

const getLoadsByUserId = async (userId,offset=0,limit=10,status) => {
    const user = await getUserById(userId);
    if(user.role==="DRIVER") {
        loads = await Load.find({assignedTo:userId});
    } else {
        loads = await Load.find({createdBy:userId});
    }
    if(status) {
        loads = loads.filter(value => value.status === status);
    }
    return loads.slice(Number(offset),Number(offset)+Number(limit));
}

const getLoadByIdForUser = async (id) => {
    const load = await Load.findById(id);
    if(!load) {
        throw new Error("No load with such id found");
    }
    return load;
}

const addLoadToUser = async (userId, loadPayload) => {
    const load = new Load({...loadPayload, createdBy:userId, logs:[{
        message: "Load created",
        time: new Date()
    }]});
    await load.save();
}

const updateLoadByIdForUser = async (id,data) => {
    await getLoadByIdForUser(id);
    await Load.findByIdAndUpdate(id, { $set: data});
}

const deleteLoadByIdForUser = async (id) => {
    await getLoadByIdForUser(id)
    await Note.findByIdAndRemove(id);
}

const postLoadByIdAndSearchDriver = async (loadId, userId) => {
    const load = await getLoadByIdForUser(loadId);
    await Load.findByIdAndUpdate(loadId,{ $set: {status: "POSTED",
        logs:[...load.logs,
        {
            message: "Load posted",
            time: new Date()
        }]}});
    let truck = null;
    const trucks = await Truck.find({status:"IS"});
    for(let i=0;i<trucks.length;i++) {
        const interatedTruck = trucks[i];
        let payload,width,length,height;
        switch (interatedTruck.type) {
            case "SPRINTER":
                payload = 1700;
                length = 300;
                width = 250;
                height = 170;
                break;
            case "SMALL STRAIGHT":
                payload = 2500;
                length = 500;
                width = 250;
                height = 170;
                break;
            case "LARGE STRAIGHT":
                payload = 4000;
                length = 700;
                width = 350;
                height = 200;
                break;
            default:
                throw new Error("Unknown truck type");
        }
        if(interatedTruck.assignedTo && payload > load.payload && length > load.dimensions.length
            && width > load.dimensions.width && height > load.dimensions.height){
            truck = trucks[i];
            break
        }
    }
    if(!truck) {
        await Load.findByIdAndUpdate(loadId,{ $set: {status: "NEW",
        logs:[...load.logs,
        {
            message: "Driver was not found",
            time: new Date()
        }]}});
        return false;
    }
    await Truck.findByIdAndUpdate(truck._id, { $set: {status: "OL"}});
    await Load.findOneAndUpdate({_id: loadId}, { $set: {
        status: "ASSIGNED",
        state: "En route to Pick Up",
        assignedTo: truck.assignedTo,
        logs:[...load.logs,
        {
            message: `Assigned to driver with id: ${truck.assignedTo}`,
            time: new Date()
        }]
    }});
    return true;
}

const getActiveLoadForUser = async (userId) => {
    const load = await Load.findOne({assignedTo:userId});
    if(!load) {
        throw new Error("You don`t have active load");
    }
    return load;
}

const iterateLoadState = async (userId) => {
    let newState;
    const load = await getActiveLoadForUser(userId);
    switch (load.state) {
        case "En route to Pick Up":
            newState = "Arrived to pick up";
            await Load.findByIdAndUpdate(load._id, { $set: {
            logs:[...load.logs,
                {
                    message: `Arrived to pick up`,
                    time: new Date()
                }]}});
            break;
        case "Arrived to pick up":
            newState = "En route to delivery";
            await Load.findByIdAndUpdate(load._id, { $set: {
            logs:[...load.logs,
                {
                    message: `En route to delivery`,
                    time: new Date()
                }]}});
            break;
        case "En route to delivery":
            newState = "Arrived to delivery";
            await Load.findByIdAndUpdate(load._id, { $set: {status: "SHIPPED",
            logs:[...load.logs,
                {
                    message: `Arrived to delivery`,
                    time: new Date()
                }]}});
            await Truck.findOneAndUpdate({assignedTo:load.assignedTo}, { $set: {status: "IS"}});
            break;
        default:
            throw new Error("Load is already delivered");
    }
    await Load.findByIdAndUpdate(load._id, { $set: {state: newState}});
    return newState;
}

const getShippingInfoByIdForUser = async (id) => {
    const load = await getLoadByIdForUser(id);
    const truck = await Truck.findOne({assignedTo:load.assignedTo});
    return {
        load,
        truck
    }
}

module.exports = {
    addLoadToUser,
    getLoadsByUserId,
    postLoadByIdAndSearchDriver,
    getActiveLoadForUser,
    iterateLoadState,
    getLoadByIdForUser,
    updateLoadByIdForUser,
    deleteLoadByIdForUser,
    getShippingInfoByIdForUser
};