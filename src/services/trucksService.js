const {Truck} = require('../models/truckModel');

const addTruckToUser = async (userId, truckPayload) => {
    const truck = new Truck({...truckPayload, createdBy:userId});
    await truck.save();
}

const getTrucksByUserId = async (userId) => {
    const trucks = await Truck.find({createdBy:userId}).select('-__v');
    return trucks;
}

const assignTruckByIdForUser = async (truckId, userId) => {
    const assignedTruck = await Truck.findOne({assignedTo:userId});
    if(assignedTruck) {
        throw new Error(`You already have assigned a truck with id:${assignedTruck._id}!`);
    }
    await getTruckByIdForUser(truckId,userId);
    await Truck.findByIdAndUpdate(truckId, { $set: { assignedTo:userId }});
}

const getTruckByIdForUser = async (id,userId) => {
    const truck = await Truck.findOne({_id:id ,createdBy:userId}).select('-__v');
    if(!truck) {
        throw new Error('Truck not found');
    }
    return truck;
}

const updateTruckByIdForUser = async (id,userId,data) => {
    await getTruckByIdForUser(truckId,userId);
    await Truck.findByIdAndUpdate(id, { $set: data});
}

const deleteTruckByIdForUser = async (id,userId) => {
    await getTruckByIdForUser(truckId,userId);
    await Truck.findByIdAndDelete(id);
}

module.exports = {
    addTruckToUser,
    getTrucksByUserId,
    assignTruckByIdForUser,
    getTruckByIdForUser,
    updateTruckByIdForUser,
    deleteTruckByIdForUser
};